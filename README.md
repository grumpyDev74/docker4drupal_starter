# docker4drupal starter

A repo to clone to start installing necessary docker4drupal

Firs create a new project using this command line :

```
composer create-project grumpydev74/docker4drupal_starter --repository-url='{"type": "git","url": "https://gitlab.com/grumpyDev74/docker4drupal_starter.git"}' --remove-vcs
```

To start your project you should configure the .env file to fit your needs
